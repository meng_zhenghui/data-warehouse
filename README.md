# data_warehouse

#### 介绍
数据仓库

#### 软件架构
软件架构说明
MTV架构
框架结构，MVT模型
model-views-template
| - apps  //程序包
    |- __init__.py //注册蓝图，初始化数据库	
        | - case  //功能模块包 ，可以有多个这种包，每个对应不同的功能
            |- temp.py //临时文件，用来读取Excel
            |- views.py //定义路由跟功能
            |- models.py //数据库模型
|- exts //常用工具包
    |- init //初始化SQLAlchemy，防止重复导入
    |- log //配置日志信息
    |- read_excel //读取Excel
|- log //记录日志文件
|- static //css,js 图片等静态文件
| - templates //jinjia2模板，静态网页模板
|- migrations //数据迁移文件夹
|- env  //虚拟环境
|- requirements.txt //列出了所有依赖包以及版本号，方便在其他位置生成相同的虚拟环境以及依赖
|- setting.py //全局配置文件
|- app.py //启动程序入口

#### 安装教程

1.  pip install -r requirements.txt
2.  settings.py，修改数据库配置
3.  初始化数据库，在cmd运行依次执行python app.py db inti初始化，python app.py db migrate迁移，python app.py db upgrade升级
3.  python app.py runserver


#### 使用说明

1.  在apps添加对应的功能模块
2.  在功能模块的路由下绑定蓝图，cases_bp= Blueprint('case',__name__)
3.  在apps.init下注册蓝图，app.register_blueprint(cases_bp)
4.  更新数据库模型后，在cmd运行依次执行python app.py db migrate迁移，python app.py db upgrade升级
5.  启动程序，python app.py runserver


 

