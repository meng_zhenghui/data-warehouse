# -*- coding: UTF-8 -*-
"""
@Project ：flaskProject
@File    ：models.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/13 16:19
"""

from datetime import datetime

from exts import db


class User(db.Model):
    """用户表"""
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(32), nullable=False, unique=True)
    password = db.Column(db.String(256), nullable=False)
    phone = db.Column(db.String(11))
    email = db.Column(db.String(30))
    icon = db.Column(db.String(100))
    isdelete = db.Column(db.Boolean, default=False)
    creat_time = db.Column(db.DateTime, default=datetime.now)
