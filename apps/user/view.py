# -*- coding: UTF-8 -*-
"""
@Project ：flaskProject
@File    ：view.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/24 15:32
"""

import functools
import json

from flask import Blueprint
from flask import request, jsonify
from flask_restful import fields, marshal_with, Resource
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature
from sqlalchemy import exc
from sqlalchemy.sql import exists
from werkzeug.security import check_password_hash, generate_password_hash

from apps.user.models import User
from exts import db, api
from exts.log import Logger
from settings import config

user_bp = Blueprint('user', __name__, url_prefix='/api/user')
SECRET_KEY = config.SECRET_KEY
log = Logger("user").write_log()


def create_token(api_user):
    """
    生成token
    :param api_user:用户id
    :return: token
    """

    # 第一个参数是内部的私钥，这里写在共用的配置信息里了，如果只是测试可以写死
    # 第二个参数是有效期(秒)
    s = Serializer(secret_key=SECRET_KEY, expires_in=3600)
    # 接收用户id转换与编码
    token = s.dumps({"id": api_user}).decode("ascii")
    return token


# 判断是否需要登录装饰器
def login_required(view_func):
    @functools.wraps(view_func)
    def verify_token(*args, **kwargs):
        try:
            token = request.headers['token']
        except Exception:
            return jsonify({"code": 401, "message": "先请登录"}), 401
        s = Serializer(SECRET_KEY)
        try:
            user = s.loads(token)
        except SignatureExpired:
            return jsonify({"code": 401, "message": "登录已经过期"}), 401
        except BadSignature:
            return jsonify({"code": 401, "message": "token错误"}), 401
        id = user.get('id')
        users = db.session.query(exists().where(User.id == id)).scalar()
        if users:
            return view_func(id, *args, **kwargs)
        else:
            return jsonify({"code": 401, "message": "token对应的用户不存在"}), 401

    return verify_token
@user_bp.route('debug')
def debug():
    @login_required
    def test(user_id, test='hui'):
        # user = User()
        user_name = db.session.query(User.username).filter(User.id == user_id).first()[0]
        # user_name = user.username
        return {"user": "{}, name = {}".format(user_name, test)}
    return test()


# 注册
@user_bp.route('/register', methods=['POST'])
def register():
    try:
        if request.method == 'POST':
            data = dict(request.get_json())
            username = data.get('username')
            password = data.get('password')
            repassword = data.get('repassword')
            phone = data.get('phone')
            much = [username, password, repassword]
            if None or '' in much:
                return jsonify({"code": -3, "msg": "必填参数不能为空"})
            else:
                if password == repassword:
                    # 注册用户
                    user = User()
                    user.username = username
                    # 使用自带的函数实现加密：generate_password_hash
                    user.password = generate_password_hash(password)
                    user.phone = phone
                    # 添加并提交
                    db.session.add(user)
                    db.session.commit()
                    msg = '注册成功'
                    log.info(msg)
                    return jsonify({"code": 0, "msg": msg})
                else:
                    return jsonify({"code": -2, "msg": "两次密码不一致"})
    except exc.IntegrityError:
        msg = "用户已存在"
        log.error(msg)
        return jsonify({"code": -4, "msg": msg}), 201
    except Exception:
        msg = "用户注册失败"
        log.error(msg, exc_info=True)
        return jsonify({"code": 500, "msg": msg}), 500


# 登录
@user_bp.route('/login', methods=['POST'])
def login():
    try:
        if request.method == 'POST':
            data = dict(request.get_json())
            username = data.get('username')
            password = data.get('password')
            print(username, password)
            if None or '' in [username, password]:
                return jsonify({"code": -3, "msg": "必填参数不能为空"})
            else:
                # 关键  select * from user where username='xxxx';
                users = User.query.filter(User.username == username).all()
                for user in users:
                    # 如果flag=True表示匹配，否则密码不匹配
                    flag = check_password_hash(user.password, password)
                    if flag:
                        token = create_token(user.id)
                        msg = "用户登录成功"
                        log.info(msg)
                        return jsonify({"code": "0", "msg": msg, "token": token})
                    else:
                        return jsonify({"code": -1, "msg": "用户名或者密码有误！"})
        else:
            return jsonify({"code": -2, "msg": "请求方式错误，请用post"}), 404
    except KeyError as e:
        return jsonify({"code": -3, "msg": "缺少必填参数{}".format(e)})
    except Exception:
        msg = "用户登录失败，系统出错"
        log.error(msg, exc_info=True)
        return jsonify({"code": 500, "msg": msg})


# 定义查看用户列表的格式
user_fields = {
    "id": fields.Integer,
    "username": fields.String,
    "phone": fields.String(default=''),
    "creat_time": fields.String(default='')
}


class UserResource(Resource):
    # 查看用户列表

    @marshal_with(user_fields)
    @login_required
    def get(self, id):
        try:
            users_db = User()
            users = users_db.query.all()
            return users
        except Exception as e:
            log.exception(e)
            msg = "查询失败"
            return jsonify({"code": 500, "msg": msg}), 500

    # 用户信息更新
    @staticmethod
    # @login_required
    def post():
        try:
            if request.method == 'POST':
                username = request.form.get('username')
                phone = request.form.get('phone')
                id = request.form.get('id')
                # 找用户
                user = User.query.get(id)
                # 改用户信息
                user.phone = phone
                user.username = username
                # 提交
                db.session.commit()
                msg = "修改用户信息成功"
                log.info(msg)
                return jsonify({"code": 0, "msg": msg})
        except Exception:
            msg = "修改用户信息失败，系统出错"
            log.error(msg)
            return jsonify({"code": 500, "msg": msg})

    # 用户删除
    @staticmethod
    def delete():
        try:
            # 获取用户id
            id = request.form.get('id')
            # 1. 逻辑删除 （更新）
            # # 获取该id的用户
            # user = User.query.get(id)
            # # 逻辑删除：
            # user.isdelete = True
            # # 提交
            # db.session.commit()
            # 2. 物理删除
            user = User.query.get(id)
            # 将对象放到缓存准备删除
            db.session.delete(user)
            # 提交删除
            db.session.commit()
            msg = "删除用户成功"
            log.info(msg)
            return jsonify({"code": 0, "msg": msg})
        except Exception:
            msg = "删除用户失败，找不到用户id"
            log.error(msg)
            return jsonify({"code": 500, "msg": msg})
            # return jsonify({"code": 0, "msg": msg})

# 通过同一个路由，不同的方法GET/POST/DELETE实现不同的操作
api.add_resource(UserResource, '/api/user')
