# -*- coding: UTF-8 -*-
"""
@Project ：BI
@File    ：models.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/26 11:14
"""
from datetime import datetime

from exts import db


class Test_cases(db.Model):
    """数据源的用例表,唐"""
    """'需求名称', '所属模块', '严重程度', '测试负责人', '开发负责人', '重现步骤', '用例编号', '来源数据库信息', '目标数据库信息', '对比库', '用例描述', '来源SQL', 
    '目标SQL', '关联条件', '检查字段', '重复明细SQL', '测试类型', '结果', '执行按钮', '对比数据量的误差比例（需提供默认值）', '差异条数（需提供默认值）' """
    __tablename__ = 'test_cases'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    need_name = db.Column(db.String(64), nullable=True)
    module = db.Column(db.String(64), nullable=True)
    severity = db.Column(db.String(64), nullable=True)
    test_user = db.Column(db.String(64), nullable=True)
    dev_user = db.Column(db.String(64), nullable=True)
    procedure = db.Column(db.String(512), nullable=True)
    case_id = db.Column(db.String(64), nullable=True)
    source_database = db.Column(db.String(64), nullable=False)
    target_database = db.Column(db.String(64), nullable=False)
    contrast_database = db.Column(db.String(64), nullable=False)
    case_msg = db.Column(db.String(64), nullable=True)
    source_sql = db.Column(db.String(512), nullable=False)
    target_sql = db.Column(db.String(512), nullable=False)
    join_condition = db.Column(db.String(512), nullable=True)
    check_type = db.Column(db.String(512), nullable=True)
    repetition_sql = db.Column(db.String(512), nullable=True)
    test_type = db.Column(db.String(64), nullable=False)
    result = db.Column(db.String(64), nullable=True)
    is_run = db.Column(db.String(64), nullable=True)
    error_range = db.Column(db.String(16), server_default='10', nullable=True)
    difference_num = db.Column(db.String(16), server_default='100', nullable=True)
    creat_time = db.Column(db.DateTime, default=datetime.now)
