# -*- coding: UTF-8 -*-
"""
@Project ：BI
@File    ：view.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/26 11:14
"""
import datetime
import os
import time

import requests
from flask import Blueprint, request, render_template, jsonify, send_from_directory
from flask_restful import fields, Resource, marshal_with

from apps.case.models import Test_cases
from exts import db, api
from exts.log import Logger
from exts.read_excel import get_xls

cases_bp = Blueprint('case', __name__, url_prefix='/api')
log = Logger("case").write_log()


@cases_bp.route('/upload_cases', methods=['GET', 'POST'])
def upload_cases():
    if request.method == 'GET':
        # get请求带参数时，默认下载Excel模板
        download_case = request.args.get('case')
        if download_case:
            temp_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../static/temp')
            return send_from_directory(temp_path, 'testtemp.xlsx', as_attachment=True)
    if request.method == 'POST':
        try:
            # 对前端模板传递过来的Excel进行处理
            file = request.files['file']
            temp_path = os.path.dirname(os.path.realpath(__file__))
            # 保存到temp临时文件夹
            excel_path = os.path.join(temp_path, '../../static/temp/testcases.xlsx')
            # 读取Excel
            file.save(excel_path)
            cases = get_xls(excel_path)
            scc_num = 0
        except Exception:
            return '请选择xlsx后缀的文件'
        try:
            for case in cases:
                cases_table = Test_cases()
                # 把读取到的数据写入数据库
                """'需求名称', '所属模块', '严重程度', '测试负责人', '开发负责人', '重现步骤', '用例编号', '来源数据库信息', '目标数据库信息', '对比库', '用例描述', 
				'来源SQL', '目标SQL', '关联条件', '检查字段', '重复明细SQL', '测试类型', '结果', '是否执行', '对比数据量的误差比例（默认值80）', '差异条数（默认值100）' 
				"""
                cases_table.need_name = case[0]
                cases_table.module = case[1]
                cases_table.severity = case[2]
                cases_table.test_user = case[3]
                cases_table.dev_user = case[4]
                cases_table.procedure = case[5]
                cases_table.case_id = case[6]
                cases_table.source_database = case[7]
                cases_table.target_database = case[8]
                cases_table.contrast_database = case[9]
                cases_table.case_msg = case[10]
                cases_table.source_sql = case[11]
                cases_table.target_sql = case[12]
                cases_table.join_condition = case[13]
                cases_table.check_type = case[14]
                cases_table.repetition_sql = case[15]
                cases_table.test_type = case[16]
                cases_table.result = case[17]
                cases_table.is_run = case[18]
                # 对比数据量的误差比例（供默认值20）', '差异条数（默认值100）
                try:
                    error_range = int(case[19])
                except Exception:
                    error_range = 10
                cases_table.error_range = error_range
                try:
                    difference_num = int(case[20])
                except Exception:
                    difference_num = 100
                cases_table.difference_num = difference_num
                db.session.add(cases_table)
                db.session.commit()
                scc_num += 1
            msg = '通过Excel导入用例成功，导入数据源用例{}条'.format(scc_num)
            log.info(msg)
        except Exception:
            msg = '通过Excel导入用例失败'
            log.error(msg, exc_info=True)
        return msg
    else:
        return render_template('case/upload_case.html')


case_fields = {'id': fields.Integer, 'need_name': fields.String, 'module': fields.String, 'severity': fields.String,
               'test_user': fields.String, 'dev_user': fields.String, 'procedure': fields.String,
               'case_id': fields.String, 'source_database': fields.String, 'target_database': fields.String,
               'contrast_database': fields.String, 'case_msg': fields.String, 'source_sql': fields.String,
               'target_sql': fields.String, 'join_condition': fields.String, 'check_type': fields.String,
               'repetition_sql': fields.String, 'test_type': fields.String, 'result': fields.String,
               'is_run': fields.String, 'error_range': fields.Integer, 'difference_num': fields.Integer,
               'creat_time': fields.String}


class CsesResource(Resource):

    @marshal_with(case_fields)
    # 显示所有的用例详情
    def get(self):
        case_base = Test_cases()
        cases = case_base.query.all()
        log.info('查询用例成功')
        return cases

    # 修改用例信息
    @staticmethod
    def post():
        try:
            """‘id’，'来源数据库信息', '目标数据库信息', '对比库', '来源SQL', '目标SQL', '关联条件', '检查字段', '测试类型' """

            id = request.form.get('id')
            source_database = request.form.get('source_database')
            target_database = request.form.get('target_database')
            contrast_database = request.form.get('contrast_database')
            source_sql = request.form.get('source_sql')
            target_sql = request.form.get('target_sql')
            join_condition = request.form.get('join_condition')
            check_type = request.form.get('check_type')
            test_type = request.form.get('test_type')

            case = Test_cases.query.get(id)
            case.source_database = source_database
            case.target_database = target_database
            case.contrast_database = contrast_database
            case.source_sql = source_sql
            case.target_sql = target_sql
            case.join_condition = join_condition
            case.check_type = check_type
            case.test_type = test_type

            # 添加并且提交
            db.session.add(case)
            db.session.commit()
            msg = "修改用例成功"
            log.info(msg)
            return jsonify({"code": "0", "msg": msg})
        except Exception:
            msg = "修改用例失败"
            log.error(msg, exc_info=True)
            return jsonify({"code": "-1", "msg": msg})

    # 删除用例
    @staticmethod
    def delete():
        try:
            case = request.form.get('id')
            id = Test_cases.query.get(case)
            db.session.delete(id)
            db.session.commit()
            msg = "删除用例成功"
            log.info(msg)
            return jsonify({"code": "0", "msg": msg})
        except Exception:
            msg = "修改用例失败，id不存在"
            log.error(msg, exc_info=True)
            return jsonify({"code": "-1", "msg": msg})


@cases_bp.route('/', methods=['GET', 'POST'])
def index():
    # log.info('testinfo')
    msg = {
    "code": None,
    "data": None,
    "message": "调用货品服务异常:系统错误，请联系管理员",
    "objectType": None,
    "success": "true"
}
    return jsonify(msg)



@cases_bp.route('/task', methods=['GET', 'POST'])
def run():
    res = '系统错误！'
    ip = request.remote_addr
    start = datetime.datetime.now()
    cookie = "GATEWAY_SESSION=39FBC2ED567BCE1C639B43CCF598F484"
    datahouse = 'EDI001'
    if request.method == 'GET':
        end = request.args.get('end')
    else:
        return 'please use get method request'
    data = {
    "api": "edi.receipt.download",
    "data": {
        "start": "0",
        "end": "{}".format(end),
        "whCode": "{}".format(datahouse)
    }
}
    url = "http://wms-perf.winnermedical.com/wms-edi-sap-service/async/api/edi/wms/in"
    headers = {"cookie": "{}".format(cookie)}
    try:
        res = requests.post(url, json=data, headers=headers)
        # try:
        #     res = res.json()
        # except:
        #     res = res.text
        # api = request.url_rule
        # time.sleep(10)
        timed = datetime.datetime.now() - start
        now_time = datetime.datetime.now()
        return jsonify({ "耗时": "{}".format(timed), "ip": "{}".format(ip), "now_time": "{}".format(now_time), "data": "{}".format(data), "result": "{}".format(res)})
    except Exception as e:
        timed = datetime.datetime.now() - start
        now_time = datetime.datetime.now()
        return jsonify({ "耗时": "{}".format(timed), "ip": "{}".format(ip), "now_time": "{}".format(now_time), "data": "{}".format(data), "result": "{}".format(res+str(e))}), 500

@cases_bp.route('/add_log', methods=['GET', 'POST'])
def add_log():
    try:
        # 调用土味情话接口
        url = "https://api.uomg.com/api/rand.qinghua"
        data = requests.get(url)
        code = data.json()["code"]
        if data.status_code == 200:
            if code == 1:
                content = data.json()["content"]
                log.info(content)
            else:
                content = data.text
                log.warning(content)
        else:
            content = {"status_code" : "{}".format(data.status_code), "data" : "{}".format(data.text)}
            log.warning(content)
        print(content)
        return content
    except Exception:
        error_msg = '接口请求错误'
        log.error(error_msg, exc_info=True)
        return error_msg


# 通过同一个路由，不同的方法GET/POST/DELETE实现不同的操作
api.add_resource(CsesResource, '/api/case')
