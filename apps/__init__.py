# -*- coding: UTF-8 -*-
"""
@Project ：flaskProject
@File    ：__init__.py.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/4/30 15:40
"""

from flask import Flask
from apps.case.view import cases_bp
from apps.user.view import user_bp
from exts import db, api


def create_app(config_name):
    # 创建flask对象
    app = Flask(__name__, template_folder="../templates", static_folder="../static")
    # 加载配置
    app.config.from_object(config_name)
    # 初始化数据库
    db.init_app(app)
    # 绑定用户api
    api.init_app(app=app)
    # 注册蓝图
    app.register_blueprint(user_bp)
    app.register_blueprint(cases_bp)
    # 打印一下绑定的路由
    print(app.url_map)
    return app
