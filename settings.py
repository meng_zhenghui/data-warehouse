# -*- coding: UTF-8 -*-
"""
@Project ：flaskProject
@File    ：settings.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/21 17:21
"""

import logging


class config:
    # 数据库配置信息
    HOSTNAME = '127.0.0.1'
    PORT = '3306'
    DATABASE = 'data_warehouse'
    USERNAME = 'root'
    PASSWORD = '123456'
    DB_URI = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(USERNAME, PASSWORD, HOSTNAME, PORT, DATABASE)
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True
    SECRET_KEY = "kdjklfjkd87384hjdhjh"
    # 加这个返回的json带有中文不会乱码
    JSON_AS_ASCII = False


class dev(config):
    DEBUG = True


class production(config):
    DB_URI = 'mysql+pymysql://root:123456@127.0.0.1:3306/data_warehouse'
    DEBUG = False
    LOG_LEVEL = logging.WARNING


if __name__ == '__main__':
    print(dev.DB_URI)
