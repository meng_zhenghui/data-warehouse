# -*- coding: UTF-8 -*-
"""
@Project ：flaskProject
@File    ：read_csv.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/25 10:35
"""

# 调用读Excel的第三方库xlrd
from xlrd import open_workbook


def get_xls(xls_name, sheet_name='测试用例-示例'):  # xls_name填写用例的Excel名称 sheet_name该Excel的sheet名称
    cls = []
    # 获取用例文件路径
    xlsPath = xls_name
    # 打开用例Excel
    with open_workbook(xlsPath) as file:
        sheet = file.sheet_by_name(sheet_name)  # 获得打开Excel的sheet
        # 获取这个sheet内容行数
        nrows = sheet.nrows
        for i in range(1, nrows):  # 根据行数做循环,第一行是表头，忽略
            cls.append(sheet.row_values(i))
    return cls


if __name__ == '__main__':  # 我们执行该文件测试一下是否可以正确获取Excel中的值
    print(get_xls(r'../static\temp\testtemp.xlsx')[1][1])
