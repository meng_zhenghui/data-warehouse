# -*- coding: UTF-8 -*-
"""
@Project ：BI
@File    ：__init__.py.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/5/26 11:13
"""
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

db = SQLAlchemy()
api = Api()
