# -*- coding: utf-8 -*-
"""
@Project ：demo_unittest
@File    ：logger.py
@IDE     ：PyCharm
@Author  ：hui
@Date    ：2021/9/24  17:55
"""
import logging
from logging.handlers import TimedRotatingFileHandler
import os
import time

# 日志的文件夹路径
log_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/logs/'
if not os.path.exists(log_path):
    os.mkdir(log_path)

# 日志输出格式
log_formatter = (
    "[%(levelname)s]:"
    "[%(asctime)s]"
    "[%(filename)s:%(module)s:%(funcName)s"
    " in %(lineno)d] "
    "%(message)s"
)
LOG_MAX_BYTES = 50 * 1024 * 1024  # 日志文件大小
backup_count = 3  # 备份文件数量
interval = 1   # when的数量
when = "MIDNIGHT"  # 每天凌晨创建MIDNIGHT
# 日志输入级别
console_output_level = logging.DEBUG  # 控制台的日志级别
file_output_level = logging.INFO  # 文件的日志级别


class Logger:
    def __init__(self, logger):
        # 日志的名字
        log_name = log_path + 'log.log'
        """在logger中添加日志句柄并返回，如果logger已有句柄，则直接返回"""
        self.logger = logging.getLogger(logger)
        logging.root.setLevel(logging.DEBUG)
        if not self.logger.handlers:
            # 创建一个handler用于写入到文件
            file_handler = logging.FileHandler(log_name)
            file_handler.setLevel(file_output_level)
            # 创建一个handler用于输出到控制台
            console_handler = logging.StreamHandler()
            console_handler.setLevel(console_output_level)
            # 每天重新创建一个日志文件，最多保留backup_count份
            file_handler = TimedRotatingFileHandler(filename=os.path.join(log_path, log_name), when=when,
                                                    interval=interval, backupCount=backup_count, delay=False,
                                                    encoding='utf-8')
            # 设置日志格式
            formatter = logging.Formatter(log_formatter)
            file_handler.setFormatter(formatter)
            console_handler.setFormatter(formatter)
            # 给 logger 添加 handler
            self.logger.addHandler(file_handler)
            self.logger.addHandler(console_handler)

    def write_log(self):
        return self.logger


if __name__ == '__main__':
    log = Logger("test").write_log()
    # try:
    #     raise AttributeError('raise error')
    # except Exception as e:
    #     log.exception(e)
        # raise e
    log.debug('info')
