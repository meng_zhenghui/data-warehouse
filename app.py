from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
# from apps.user.models import *
# from apps.case.models import *
from exts import db
from apps import create_app
from settings import dev

app = create_app(dev)

manager = Manager(app=app)
migrate = Migrate(app=app, db=db)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
